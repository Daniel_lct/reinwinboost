/**
 * Main slider / Главный слайдер
 */

let mainSlider = new Swiper('.swiper-container', {
    effect: 'coverflow',
    grabCursor: true,
    initialSlide: 3,
    centeredSlides: true,
    slidesPerView: 'auto',
    coverflowEffect: {
        rotate: 25,
        stretch: 10,
        depth: 200,
        modifier: 0,
        slideShadows: false,
    },
    navigation: {
        nextEl: '.button-next',
        prevEl: '.button-prev',
    },
    breakpoints: {
        // when window width is >= 320px
        320: {
            spaceBetween: 30,
            coverflowEffect: {
                rotate: 25,
                stretch: 10,
                depth: 200,
                modifier: 0,
                slideShadows: false,
            },
        },
        // when window width is >= 480px
        480: {
            spaceBetween: 30,
            coverflowEffect: {
                rotate: 25,
                stretch: 10,
                depth: 200,
                modifier: 0,
                slideShadows: false,
            },
        },
        // when window width is >= 640px
        640: {
            spaceBetween: 0,
            coverflowEffect: {
                rotate: 25,
                stretch: 10,
                depth: 200,
                modifier: 1,
                slideShadows: false,
            },
        }
    }
})



/**
 * Subsliders / Слайдеры на втором экране (под главным)
 */

let mainSliderSubslider = new Swiper('.swiper-bestsellers', {
    effect: 'coverflow',
    grabCursor: true,
    initialSlide: 1,
    centeredSlides: true,
    slidesPerView: 'auto',
    coverflowEffect: {
        rotate: 25,
        stretch: 10,
        depth: 200,
        modifier: 0,
        slideShadows: false,
    },
    navigation: {
        nextEl: '.button-next',
        prevEl: '.button-prev',
    },
    breakpoints: {
        // when window width is >= 320px
        320: {
            spaceBetween: 30,
            coverflowEffect: {
                rotate: 25,
                stretch: 10,
                depth: 200,
                modifier: 0,
                slideShadows: false,
            },
        },
        // when window width is >= 480px
        480: {
            spaceBetween: 30,
            coverflowEffect: {
                rotate: 25,
                stretch: 10,
                depth: 200,
                modifier: 0,
                slideShadows: false,
            },
        },
        // when window width is >= 640px
        640: {
            // centeredSlides: true,
            spaceBetween: 20,
            coverflowEffect: {
                rotate: 25,
                stretch: 10,
                depth: 200,
                modifier: 0,
                slideShadows: false,
            },
        },
        768: {
            // centeredSlides: true,
            spaceBetween: 0,
            coverflowEffect: {
                rotate: 25,
                stretch: 10,
                depth: 200,
                modifier: 0,
                slideShadows: false,
            },
        }
    }
})



/**
 * Reviews slider / Слайдер блока с отзывами
 */

let reviewsSlider = new Swiper('.swiper-reviews', {
    effect: 'coverflow',
    grabCursor: true,
    initialSlide: 1,
    centeredSlides: false,
    slidesPerView: 'auto',
    coverflowEffect: {
        rotate: 25,
        stretch: 10,
        depth: 200,
        modifier: 0,
        slideShadows: false,
    },
    navigation: {
        nextEl: '.button-next',
        prevEl: '.button-prev',
    },
    breakpoints: {
        // when window width is >= 320px
        320: {
            spaceBetween: 30,
            coverflowEffect: {
                rotate: 25,
                stretch: 10,
                depth: 200,
                modifier: 0,
                slideShadows: false,
            },
        },
        // when window width is >= 480px
        480: {
            spaceBetween: 30,
            coverflowEffect: {
                rotate: 25,
                stretch: 10,
                depth: 200,
                modifier: 0,
                slideShadows: false,
            },
        },
        // when window width is >= 640px
        640: {
            // centeredSlides: true,
            spaceBetween: 20,
            coverflowEffect: {
                rotate: 25,
                stretch: 10,
                depth: 200,
                modifier: 0,
                slideShadows: false,
            },
        },
        768: {
            // centeredSlides: true,
            spaceBetween: 50,
            coverflowEffect: {
                rotate: 25,
                stretch: 10,
                depth: 200,
                modifier: 0,
                slideShadows: false,
            },
        }
    }
})



/**
 * Blog slider / Слайдер блога
 */

let blogSlider = new Swiper('.swiper-blog', {
    effect: 'fade',
    grabCursor: true,
    initialSlide: 1,
    centeredSlides: true,
    slidesPerView: 'auto',
    coverflowEffect: {
        rotate: 25,
        stretch: 10,
        depth: 200,
        modifier: 0,
        slideShadows: false,
    },
    navigation: {
        nextEl: '.button-next',
        prevEl: '.button-prev',
    },
    breakpoints: {
        // when window width is >= 320px
        320: {
            spaceBetween: 30,
            coverflowEffect: {
                rotate: 25,
                stretch: 10,
                depth: 200,
                modifier: 0,
                slideShadows: false,
            },
        },
        // when window width is >= 480px
        480: {
            spaceBetween: 30,
            coverflowEffect: {
                rotate: 25,
                stretch: 10,
                depth: 200,
                modifier: 0,
                slideShadows: false,
            },
        },
        // when window width is >= 640px
        640: {
            // centeredSlides: true,
            spaceBetween: 20,
            coverflowEffect: {
                rotate: 25,
                stretch: 10,
                depth: 200,
                modifier: 0,
                slideShadows: false,
            },
        },
        768: {
            // centeredSlides: true,
            spaceBetween: 50,
            coverflowEffect: {
                rotate: 25,
                stretch: 10,
                depth: 200,
                modifier: 0,
                slideShadows: false,
            },
        },
        1024: {
            centeredSlides: false,
        }
    }
})



/**
 * Card Slider / Слайдер в карточке товара
 */

let cardSlider

$(document).ready(function(){
    // if($(this).width() > 1024){
        cardSlider = new Swiper('.swiper-card', {
            // effect: 'coverflow',
            grabCursor: true,
            initialSlide: 2,
            centeredSlides: false,
            slidesPerView: 'auto',

            navigation: {
                nextEl: '.button-next',
                prevEl: '.button-prev',
            },
            breakpoints: {
                // when window width is >= 320px
                1024: {
                    spaceBetween: 30,
                    centeredSlides: false,
                },
                1440: {
                    slidesPerView: 'auto',
                    spaceBetween: 25,
                    centeredSlides: false,
                    initialSlide: 1,
                },
            }
        })
    // } else {
    //     cardSlider.destroy()
    // }
})




/**
 * Calendar slider / Календарь слайдер
 */

let calendarSlider = new Swiper('.calendar__slider', {
    // effect: 'coverflow',
    grabCursor: true,
    initialSlide: 2,
    centeredSlides: true,
    slidesPerView: '5',
    spaceBetween: 40,

    coverflowEffect: {
        rotate: 25,
        stretch: 10,
        depth: 200,
        modifier: 0,
        slideShadows: false,
    },
    navigation: {
        nextEl: '.button-next',
        prevEl: '.button-prev',
    },
    breakpoints: {
        // when window width is >= 320px
        320: {
            // spaceBetween: 30,
            slidesPerView: '1',
            coverflowEffect: {
                rotate: 25,
                stretch: 10,
                depth: 200,
                modifier: 0,
                slideShadows: false,
            },
        },
        768: {
            slidesPerView: 'auto',
            centeredSlides: true,
            coverflowEffect: {
                rotate: 25,
                stretch: 10,
                depth: 200,
                modifier: 0,
                slideShadows: false,
            },

        },
        1024: {
            slidesPerView: '3',
            spaceBetween: 30,
            centeredSlides: true,
            coverflowEffect: {
                rotate: 25,
                stretch: 10,
                depth: 200,
                modifier: 0,
                slideShadows: false,
            },
        },
        1440: {
            slidesPerView: '5',
            centeredSlides: true,
            coverflowEffect: {
                rotate: 25,
                stretch: 10,
                depth: 200,
                modifier: 0,
                slideShadows: false,
            },
        },

        1920: {
            slidesPerView: '7',
            spaceBetween: 20,
            initialSlide: 3,
            centeredSlides: true,
            coverflowEffect: {
                rotate: 25,
                stretch: 10,
                depth: 200,
                modifier: 0,
                slideShadows: false,
            },
        }

    }
})

/**
 * Menu calendar slider / Календарь слайдер в меню
 */

// let calendarMenuSlider = new Swiper('.nav__calendar-slider', {
//     // effect: 'coverflow',
//     grabCursor: true,
//     initialSlide: 2,
//     centeredSlides: true,
//     slidesPerView: '5',
//     spaceBetween: 40,
//
//     coverflowEffect: {
//         rotate: 25,
//         stretch: 10,
//         depth: 200,
//         modifier: 0,
//         slideShadows: false,
//     },
//     navigation: {
//         nextEl: '.button-next',
//         prevEl: '.button-prev',
//     },
//     breakpoints: {
//         // when window width is >= 320px
//         320: {
//             // spaceBetween: 30,
//             slidesPerView: '1',
//             coverflowEffect: {
//                 rotate: 25,
//                 stretch: 10,
//                 depth: 200,
//                 modifier: 0,
//                 slideShadows: false,
//             },
//         },
//         768: {
//             slidesPerView: 'auto',
//             centeredSlides: true,
//             coverflowEffect: {
//                 rotate: 25,
//                 stretch: 10,
//                 depth: 200,
//                 modifier: 0,
//                 slideShadows: false,
//             },
//
//         },
//         1024: {
//             slidesPerView: '1',
//             centeredSlides: true,
//             coverflowEffect: {
//                 rotate: 25,
//                 stretch: 10,
//                 depth: 200,
//                 modifier: 0,
//                 slideShadows: false,
//             },
//         },
//         1440: {
//             slidesPerView: '2',
//             centeredSlides: true,
//             coverflowEffect: {
//                 rotate: 25,
//                 stretch: 10,
//                 depth: 200,
//                 modifier: 0,
//                 slideShadows: false,
//             },
//         },
//
//         1920: {
//             slidesPerView: '7',
//             spaceBetween: 20,
//             initialSlide: 3,
//             centeredSlides: true,
//             coverflowEffect: {
//                 rotate: 25,
//                 stretch: 10,
//                 depth: 200,
//                 modifier: 0,
//                 slideShadows: false,
//             },
//         }
//
//     }
// })