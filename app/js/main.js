/**
 * Функция для добавление в избранное
 */

/* Главная страница */

$(document).ready(function(){
    $('.slider-card__btn-heart').each(function(){
        $(this).on("click", function(e){
            e.preventDefault()
            $(this).parents(".swiper-slide").toggleClass("swiper-slide_active")
            $(this).find(".slider-card__svg").toggleClass("slider-card__svg_active")
        })
    })
})


/* Страница каталога*/

$(document).ready(function(){
    $('.catalog-card__svg').each(function(){
        $(this).on("click", function(e){
            e.preventDefault()
            $(this).parents(".catalog__card").toggleClass("catalog__card_active")
            $(this).find(".catalog-card__path").toggleClass("catalog-card__path_active")
        })
    })
})


/* Страница карточка товара*/

$(document).ready(function(){
    $('.card-single__svg').each(function(){
        $(this).on("click", function(e){
            e.preventDefault()
            $(this).parents(".card-single").toggleClass("card-single_active")
            $(this).find(".card-single__path").toggleClass("card-single__path_active")
        })
    })
})




/**
 * Функция для открытия - закрытия меню (secondary)
 */

$(document).ready(function(){
    $('.header__icon-menu').on("click", function(){
        $('.nav-secondary__submenu').toggleClass('nav-secondary__submenu_active')
        $('.header__icon-menu-path').toggleClass('header__icon-menu-path_active')
    })
})



/**
 * Отчистка поля ввода search
 */


$(document).ready(function(){
    $('.header__nav-search-close').on('click', function(){
        $('.header__nav-search-input').val('')
    })
})



/**
 * Переключение вкладок меню каталога (Десктопная версия)
 */

$(document).ready(function(){


    $('[name="nav__input"]').change(function(){

        if(this.checked){
            $('[name="nav__input"]').not(this).prop('checked', false);
        }
    });

    $('[name="nav__input-sub"]').change(function(){

        if(this.checked){
            $('[name="nav__input-sub"]').not(this).prop('checked', false);
        }
    });

    $('[name="nav__input-sub-sub"]').change(function(){

        if(this.checked){
            $('[name="nav__input-sub-sub"]').not(this).prop('checked', false);
        }
    });


});





/**
 * Функция для открытия пунктов меню / the function to open the menu's items
 */

$(document).ready(function(){
    $('.header-link__subblock').each(function(){
        $(this).on('click', function(){
            $(this).find('.header__play-sublink').toggleClass('header__play-sublink_active')
            $(this).toggleClass('header-link__subblock_active')
            $(this).find('.header__play-subarrow').toggleClass('header__play-subarrow_active')
            $(this).parents('.header__play-subitem').find('.header__play-sublist').toggleClass('header__play-sublist_active')
        })
    })

    $('.header__play-subitem').each(function(){
        $(this).on('click', function(){
            $(this).parents('.header-link__subblock').find('.header__play-sublink').toggleClass('header-link__subblock_active')
            // $(this).find('.header__play-thirdlist').toggleClass('header__play-sublist_active')
        })
    })

    
    $('.nav-secondary__item').each(function(){
        $(this).on('mouseover', function(){
            $('.nav-secondary__dropdown').removeClass('nav-secondary__submenu_active')
            $(this).find('.nav-secondary__dropdown').toggleClass('nav-secondary__submenu_active')
        })

        $(this).on('mouseout', function(){
            $('.nav-secondary__dropdown').removeClass('nav-secondary__submenu_active')
        })

    })
})




/**
 * Функция для поля select. Добавление и удаление клчюевых слов
 */

$(document).ready(function(){

    $(document).on('click', function (e){
        if(e.target.className === 'select__game-item'){
            e.target.remove()
        }
    })

    $('select').each(function(){
        var $this = $(this), numberOfOptions = $(this).children('option').length;

        $this.addClass('select-hidden');
        $this.wrap('<div class="select"></div>');
        $this.after('<div class="select-styled"></div>');

        var $styledSelect = $this.next('div.select-styled');
        // $styledSelect.text($this.children('option').eq(0).text());
        $styledSelect.text($this.children('option').eq(0).text());

        var $list = $('<ul />', {
            'class': 'select-options'
        }).insertAfter($styledSelect);

        for (var i = 0; i < numberOfOptions; i++) {
            $('<li />', {
                text: $this.children('option').eq(i).text(),
                rel: $this.children('option').eq(i).val()
            }).appendTo($list);
        }

        var $listItems = $list.children('li');

        $styledSelect.click(function(e) {
            e.stopPropagation();
            $('div.select-styled.active').not(this).each(function(){
                $(this).removeClass('active').next('ul.select-options').hide();
            });
            $(this).toggleClass('active').next('ul.select-options').toggle();

        });

        $listItems.click(function(e) {
            e.stopPropagation();
            $styledSelect.text($(this).text()).removeClass('active');
            $this.val($(this).attr('rel'));
            $list.hide();
            $(this).parents('.catalog__select-wrapper').find('.select__game-results').append(`<p class="select__game-item">${$(this).text()}</p>`)
            $styledSelect.text($this.children('option').eq(0).text());
            $(this).parents('.header__currency, .catalog__sortby, .catalog__limit, .card__currency, .sign-in__contact-block').find('.select-styled').text($(this).text())

        });

        $(document).click(function() {
            $styledSelect.removeClass('active');
            $list.hide();
        });

    });
})

/**
 * Функция для input type="range"
 */


// maps a value from one range to another
Number.prototype.map = function (inMin, inMax, outMin, outMax) {
    return (this - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
}

// remaps a value from a range to a logarithmic scale
Number.prototype.mapLog = function (min, max) {
    const mapped = (this - min) * (Math.log(max) - Math.log(min)) / (max - min) + Math.log(min);
    return Math.exp(mapped);
}

// remaps a value from a range to a logarithmic scale (reversed)
Number.prototype.mapLogRev = function (min, max) {
    return (Math.log(this) - Math.log(min)) * (max - min) / (Math.log(max) - Math.log(min)) + Math.log(min);
    //return (Math.log(this) - Math.log(min)) / (Math.log(max) - Math.log(min)) / (max - min) + min;
}

// -------------------------------------------
// Variables
// -------------------------------------------

let MIN = 10;
let MAX = 100000;
let initFrom  = 0;
let initTo  = 1000;

const $logSlider = $('.range-slider.logarithmic');
const $inputMin = $('.input-min');
const $inputMax = $('.input-max');
const $inputFrom = $('.input-from');
const $inputTo = $('.input-to');

// -------------------------------------------
// Initializing
// -------------------------------------------

// min
$inputMin.attr('placeholder', `min $${MIN.toLocaleString()}`);
$inputMin.attr('data-value', MIN);

// max
$inputMax.attr('placeholder', `max $${MAX.toLocaleString()}`);
$inputMax.attr('data-value', MAX);

// from
$inputFrom.attr('placeholder', `from $${initFrom.toLocaleString()}`);
$inputFrom.attr('data-value', initFrom);
$inputFrom.attr('min', MIN);
$inputFrom.attr('max', MAX);

// to
$inputTo.attr('placeholder', `up to $${initTo.toLocaleString()}`);
$inputTo.attr('data-value', initTo);
$inputTo.attr('min', MIN);
$inputTo.attr('max', MAX);

// slider
$logSlider.ionRangeSlider({
    type: 'double',
    grid: false,
    min: mapLogarithmicReverse(MIN),
    max: mapLogarithmicReverse(MAX),
    from: mapLogarithmicReverse(initFrom),
    to: mapLogarithmicReverse(initTo),
    // prettify: prettifyLog,
    prefix: '',
    postfix: '',
    onStart: printSliderData,
    onChange: printSliderData,
    onUpdate: printSliderData
});


// -------------------------------------------
// Helper functions
// -------------------------------------------

function mapLogarithmic(value){
    return Math.round(value.mapLog(MIN, MAX));
}

function mapLogarithmicReverse(value){
    return Math.round(parseFloat(value).mapLogRev(MIN, MAX));
}

function prettifyLog(value) {
    return mapLogarithmic(value).toLocaleString('en-US');
}

// -------------------------------------------
// Event Handler
// -------------------------------------------

// slider
function printSliderData(){
    const prettyFrom = mapLogarithmic($logSlider.data('from'));
    const prettyTo = mapLogarithmic($logSlider.data('to'));

    $('.output-from').text(prettyFrom);
    $('.output-to').text(prettyTo);
}

// number inputs
$('.number-input').on('focus', e => {
    const currentTarget = e.currentTarget;
    currentTarget.value = currentTarget.dataset.value;
});

$('.number-input').on('change', e => {
    const min = $inputMin.val() || $inputMin.attr('data-value');
    const max = $inputMax.val() || $inputMax.attr('data-value');
    const from = $inputFrom.val() || $inputFrom.attr('data-value');
    const to = $inputTo.val() || $inputTo.attr('data-value');

    $logSlider.data('ionRangeSlider').update({
        min: mapLogarithmicReverse(min),
        max: mapLogarithmicReverse(max),
        from: mapLogarithmicReverse(from),
        to: mapLogarithmicReverse(to)
    });
});

$('.number-input').on('blur', e => {
    const currentTarget = e.currentTarget;
    const prefix = currentTarget.dataset.prefix;
    const localizedValue = Math.trunc(currentTarget.value).toLocaleString();

    currentTarget.placeholder = `${prefix} $${localizedValue}`;
    currentTarget.dataset.value = currentTarget.value;
    currentTarget.value = '';
});


/**
 * Функция для сброса фильтра
 */

$(document).ready(function(){
    $('.catalog__apply-svg rect').on('click', function(){
        $('.select__game-item').each(function(){
            $(this).remove()
            $('input:checked').prop('checked', false);
        })
    })
})


/**
 * Функция показа сообщения Not found и скрытие блока фильтр на мобильной версии
 */

$(document).ready(function(){
    function checkIfNotFound(){
        if($('.catalog__notfound').has('catalog__notfound_active')){
            if($(window).width() < 1024){
                $('.catalog__sidebar, .catalog__cards').hide()
            } else {
                $('.catalog__sidebar, .catalog__cards').show()
            }
        }
    }

    // checkIfNotFound()

})


/**
 * Функция календаря в карточке товара
 */

$(function() {
    $( ".card__calendar-dropdown" ).datepicker({
        dateFormat: 'dd/mm/yy',
        firstDay: 1
    });

    $(document).on('click', '.card__picker-input', function(e){
        e.preventDefault()
        var $me = $(this),
            $parent = $me.parents('.card__date-picker').find('.card__calendar-dropdown');
        $parent.toggleClass('open');
    });


    $(".card__calendar-dropdown").on("change",function(){
        var $me = $(this),
            $selected = $me.val(),
            $parent = $me.parents('.card__date-picker');
        $parent.find('.card__calendar-result').children('span').html($selected);
    });
});


/**
 * Табы
 */

const [tabs, tabsPanels] = [
    Array.from(document.querySelectorAll(".card__tabs li")),
    Array.from(document.querySelectorAll(".card__tabs-panel"))
];

tabs.forEach((tab) => {
    tab.addEventListener("click", () => {
        const target = document.querySelector(`#${tab.dataset.target}`);
        removeActiveClass([tabs, tabsPanels]);
        tab.classList.add("active");
        target.classList.add("active");
    });
});

const removeActiveClass = (el) => {
    el.forEach((item) => {
        item.find((e) => e.classList.contains("active")).classList.remove("active");
    });
};


/**
 * Настройки loot - в карточке товара
 */

$(document).ready(function(){
    var sheet = document.createElement('style'),
        $rangeInput = $('.card__loot-range input'),
        prefs = ['webkit-slider-runnable-track', 'moz-range-track', 'ms-track'];

    document.body.appendChild(sheet);

    var getTrackStyle = function (el) {
        var curVal = el.value,
            val = (curVal - 1) * 11,
            style = '';

        // Set active label
        $('.range-labels li').removeClass('active selected');

        var curLabel = $('.range-labels').find('li:nth-child(' + curVal + ')');

        curLabel.addClass('active selected');
        curLabel.prevAll().addClass('selected');

        // Change background gradient
        for (var i = 0; i < prefs.length; i++) {
            style += '.card__loot-range {background: linear-gradient(to right, #fff 0%, #fff ' + val + '%, #111111 ' + val + '%, #111111 100%)}';
            style += '.card__loot-range input::-' + prefs[i] + '{background: linear-gradient(to right, #fff 0%, #fff ' + val + '%, #414141 ' + val + '%, #414114 100%)}';
        }

        return style;
    }

    $rangeInput.on('input', function () {
        sheet.textContent = getTrackStyle(this);
    });

    // Change input value on label click
    $('.range-labels li').on('click', function () {
        var index = $(this).index();

        $rangeInput.val(index + 1).trigger('input');

    });
})



/**
 * Настройки loot - на странице checkout
 */

$(document).ready(function(){
    var sheet = document.createElement('style'),
        $rangeInput = $('.checkout-true__loot-range input'),
        prefs = ['webkit-slider-runnable-track', 'moz-range-track', 'ms-track'];

    document.body.appendChild(sheet);

    var getTrackStyle = function (el) {
        var curVal = el.value,
            val = (curVal - 1) * 11,
            style = '';

        // Set active label
        $('.range-labels li').removeClass('active selected');

        var curLabel = $('.range-labels').find('li:nth-child(' + curVal + ')');

        curLabel.addClass('active selected');
        curLabel.prevAll().addClass('selected');

        // Change background gradient
        for (var i = 0; i < prefs.length; i++) {
            style += '.card__loot-range {background: linear-gradient(to right, #fff 0%, #fff ' + val + '%, #111111 ' + val + '%, #111111 100%)}';
            style += '.card__loot-range input::-' + prefs[i] + '{background: linear-gradient(to right, #fff 0%, #fff ' + val + '%, #414141 ' + val + '%, #414114 100%)}';
        }

        return style;
    }

    $rangeInput.on('input', function () {
        sheet.textContent = getTrackStyle(this);
    });

    // Change input value on label click
    $('.range-labels li').on('click', function () {
        var index = $(this).index();

        $rangeInput.val(index + 1).trigger('input');

    });
})


/**
 * Range slider в карточке товара с пунктами
 */

const hideCirclesInRangeInputRank = function(){
    document.querySelectorAll('.card__rank .irs-grid-pol').forEach(i=>{
        if(i.className === 'irs-grid-pol'){
            if(Number(i.style.left.slice(0, -1)) >= document.querySelector('.from').style.left.slice(0,-1)){
                i.style.background = '#CF4242'
                i.nextElementSibling.classList.add('irs--active-text')

                if(Number(i.style.left.slice(0, -1)) <= document.querySelector('.to').style.left.slice(0,-1)){
                    i.style.background = '#CF4242'
                    i.nextElementSibling.classList.add('irs--active-text')


                } else {
                    i.style.background = ''
                    i.nextElementSibling.classList.remove('irs--active-text')

                }
            } else {
                i.style.background = ''
                i.nextElementSibling.classList.remove('irs--active-text')

            }

            if(document.querySelector('.card__rank .irs-to').textContent === '5 000'){
                document.querySelector('.js-grid-text-10').style.color = '#fff'
            } else {
                document.querySelector('.js-grid-text-10').style.color = '#999'
            }
        }

    })
}

const hideCirclesInRangeInputLevel = function(){
    document.querySelectorAll('.card__level .irs-grid-pol').forEach(i=>{
        if(i.className === 'irs-grid-pol'){
            if(Number(i.style.left.slice(0, -1)) >= document.querySelector('.card__level .from').style.left.slice(0,-1)){
                i.style.background = '#CF4242'
                i.nextElementSibling.classList.add('irs--active-text')

                if(Number(i.style.left.slice(0, -1)) <= document.querySelector('.card__level .to').style.left.slice(0,-1)){
                    i.style.background = '#CF4242'
                    i.nextElementSibling.classList.add('irs--active-text')
                } else {
                    i.style.background = ''
                    i.nextElementSibling.classList.remove('irs--active-text')
                }
            } else {
                i.style.background = ''
                i.nextElementSibling.classList.remove('irs--active-text')
            }

            if(document.querySelector('.card__level .irs-to').textContent === '100'){
                document.querySelector('.card__level .js-grid-text-10').style.color = '#fff'
            } else {
                document.querySelector('.card__level .js-grid-text-10').style.color = '#999'
            }
        }

        document.querySelector('#card__level-current').textContent = document.querySelector('.card__level .irs-from').textContent
        document.querySelector('#card__level-desired').textContent = document.querySelector('.card__level .irs-to').textContent
    })
}

const highlightNumberInRangeMythic = function(){
    document.querySelectorAll('.card__mythic .irs-grid-pol').forEach(i=>{
        if(i.className === 'irs-grid-pol'){
            if(Number(i.style.left.slice(0, -1)) -10 <= document.querySelector('.card__mythic .irs-single').style.left.slice(0,-1)){
                i.style.background = '#CF4242'
                i.nextElementSibling.classList.add('irs--active-text')

            } else {
                i.style.background = ''
                i.nextElementSibling.classList.remove('irs--active-text')
            }

            if(document.querySelector('.card__mythic .irs-single').textContent === '20'){
                document.querySelector('.card__mythic .js-grid-text-10').style.color = '#fff'
            } else {
                document.querySelector('.card__mythic .js-grid-text-10').style.color = '#999'
            }
        }

    })


}


$(document).ready(function(){
    hideCirclesInRangeInputRank()
    hideCirclesInRangeInputLevel()
    highlightNumberInRangeMythic()
})

$("#card_rank").ionRangeSlider({
    type: "double",
    grid: true,
    from: 500,
    to: 2000,
    min: 0,
    max: 5000,
    step: 1,
    grid_num: 10,
    onChange(){
        hideCirclesInRangeInputRank()
    }
});


/**
 * Range slider в карточке товара без пунктов (Level)
 */


$("#card__level-range").ionRangeSlider({
    type: "double",
    grid: true,
    from: 10,
    to: 30,
    min: 0,
    max: 100,
    step: 1,
    grid_num: 10,
    onChange(){
        hideCirclesInRangeInputLevel()
    }
});

/**
 * Range slider в карточке товара без пунктов (Mythic)
 */


$("#card__mythic-range").ionRangeSlider({
    type: "single",
    grid: true,
    from: 15,
    to: 18,
    min: 10,
    max: 20,
    step: 1,
    grid_num: 10,
    onChange(){
        highlightNumberInRangeMythic()
    }
});

/**
 * Калькулятор корзины
 */


$(document).ready(function(){

    let a
    let arrayTotal = []


    $('.cart__item .cart__svg-plus').on('click', function(){

        let sum = []

        let incrementNum = $(this).parents('.cart__item').find('.quantity__num').get(0).value++

        let totalPriceForOneItem = Number($(this).parents('.cart__item').find('.item__price').text().slice(1)) * (incrementNum + 1)

        $(this).parents('.cart__item').find('.item__price-total').text(`€ ${totalPriceForOneItem}`)

        arrayTotal.push(Number($(this).parents('.cart__item').find('.item__price').text().slice(1)))

        a = arrayTotal.reduce(function(a,b){
            return a + b
        })


        $('.item__price-total').each(function(){
            sum.push(Number($(this).text().slice(1)))
        })

        let test = sum.reduce(function(a,b){
            return a + b
        })

        $('.cart__subtotal-price, .cart__invoice-price').text(`€ ${test}`)


    })



    $('.cart__item .cart__svg-minus').on('click', function(){
        let sum = []


        if($(this).parents('.cart__item').find('.quantity__num').val() <= 2){
            $(this).parents('.cart__item').find('.quantity__num').val(2)
        }

        let incrementNum = $(this).parents('.cart__item').find('.quantity__num').get(0).value--

        let totalPriceForOneItem = Number($(this).parents('.cart__item').find('.item__price').text().slice(1)) * (incrementNum - 1)

        $(this).parents('.cart__item').find('.item__price-total').text(`€ ${totalPriceForOneItem}`)

        arrayTotal.push(Number($(this).parents('.cart__item').find('.item__price').text().slice(1)))

        a = arrayTotal.reduce(function(a,b){
            return a - b
        })


    })

})



/**
 * Попап для страницы checkout-false
 */

$(document).ready(function(){
    const openModalButtons = document.querySelectorAll('[data-modal-target]')
    const closeModalButtons = document.querySelectorAll('[data-close-button]')
    const overlay = document.getElementById('overlay');

    openModalButtons.forEach(button => {
        button.addEventListener('click', () => {
            const modal = document.querySelector(button.dataset.modalTarget)
            openModal(modal)
        })
    })

    closeModalButtons.forEach(button => {
        button.addEventListener('click', () => {
            const modal = button.closest('.modal'); //select parent element html
            closeModal(modal)
        })
    })

    overlay.addEventListener('click', () => {
        const modals = document.querySelectorAll('.modal.active')
        modals.forEach(modal => {
            closeModal(modal)
        })
    })

    function openModal(modal){
        // if(modal == null){
        //     return
        // }
        if(modal == null) return
        modal.classList.add('active')
        overlay.classList.add('active')
    }

    function closeModal(modal){
        // if(modal == null){
        //     return
        // }
        if(modal == null) return
        modal.classList.remove('active')
        overlay.classList.remove('active')
    }
})

/**
 * Активация выбраного дня на странице Calendar
 */

$(document).ready(function(){
    $('.calendar__slide').each(function(){
        $(this).on('click', function(){
            $(this).toggleClass('calendar__slide_active')
        })
    })
})


/**
 * Скрипт вывода полей ввода контатов на странице sign-in и checkout-false
 */

$(document).ready(function(){
    let num = 1
    $('.sign-in__contact-block .select .select-options li').on('click', function(){
        $('.sign-in__input-block.hidden').each(function(){
            if($('.sign-in__contact-block .select-styled').text().replace(/\s+/g, '').toLowerCase() == $(this).find('input').attr('id')){
                $(this).removeClass('hidden')
                $('.sign-in__contact-block label').text(`Contact ${num++}`)
                console.log(num)
                if(num === 5){
                    $('.sign-in__contact-block small').css('display', 'block')
                }
            }
        })
    })
})

/**
 * Удалить атрибут disablet у кнопки регистрации на странице Sign-in
 */

$(document).ready(function(){
    $('.sign-in__robot input').on('click', function(){
        if($(this).is(':checked')){
            $('.sign-in__register').removeAttr('disabled')
        } else {
            $('.sign-in__register').attr('disabled', 'disabled')
        }
    })
})


/**
 * Табы в Личном кабинете
 */

$(document).ready(function(){

    $('section.account__tabs h4').click(function(event) {
        event.preventDefault();
        $(this).addClass('active');
        $(this).siblings().removeClass('active');

        var ph = $(this).parent().height();
        var ch = $(this).next().height();

        if (ch > ph) {
            $(this).parent().css({
                'min-height': ch + 'px'
            });
        } else {
            $(this).parent().css({
                'height': 'auto'
            });
        }
    });

    function tabParentHeight() {
        var ph = $('section.account__tabs').height();
        var ch = $('section ul').height();
        if (ch > ph) {
            $('section.account__tabs').css({
                'height': ch + 'px'
            });
        } else {
            $(this).parent().css({
                'height': 'auto'
            });
        }
    }

    $(window).resize(function() {
        tabParentHeight();
    });

    $(document).resize(function() {
        tabParentHeight();
    });
    tabParentHeight();
})


$(document).ready(function(){

    $('section.account__tabs-inner h4').click(function(event) {
        event.preventDefault();
        $(this).addClass('active');
        $(this).siblings().removeClass('active');

        var ph = $(this).parent().height();
        var ch = $(this).next().height();

        if (ch > ph) {
            $(this).parent().css({
                'min-height': ch + 'px'
            });
        } else {
            $(this).parent().css({
                'height': 'auto'
            });
        }
    });

    function tabParentHeight() {
        var ph = $('section.account__tabs-inner').height();
        var ch = $('section ul').height();
        if (ch > ph) {
            $('section.account__tabs-inner').css({
                'height': ch + 'px'
            });
        } else {
            $(this).parent().css({
                'height': 'auto'
            });
        }
    }

    $(window).resize(function() {
        tabParentHeight();
    });

    $(document).resize(function() {
        tabParentHeight();
    });
    tabParentHeight();
})

